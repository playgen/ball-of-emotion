﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PickUpObject : MonoBehaviour {

	private Camera _cam;
	private EmotionalObject _holding;
	private EmotionCheck _emotion;

	[SerializeField]
	private LayerMask _pickUpLayers;

	private void Start()
	{
		_cam = Camera.main;
		_emotion = GetComponent<EmotionCheck>();
	}

	void Update () {
		if (_holding)
		{
			if (Input.GetKeyDown(KeyCode.I))
			{
				_holding.Waiting();
				_emotion.FakeEvent(Emotion.Anger);
			}
			if (Input.GetKeyDown(KeyCode.O))
			{
				_holding.Waiting();
				_emotion.FakeEvent(Emotion.Sad);
			}
			if (Input.GetKeyDown(KeyCode.P))
			{
				_holding.Waiting();
				_emotion.FakeEvent(Emotion.Positive);
			}
		}
		if (Input.GetKeyDown(KeyCode.R))
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			SceneManager.LoadScene("Menu");
		}
		if (!_holding && Input.GetMouseButtonDown(0))
		{
			if (_holding)
			{
				_holding.Drop();
			}
			_holding = null;
			RaycastHit hit = new RaycastHit();
			if (!Physics.Raycast(_cam.ScreenPointToRay(new Vector2(Screen.width * 0.5f, Screen.height * 0.5f)).origin,
				_cam.ScreenPointToRay(new Vector2(Screen.width * 0.5f, Screen.height * 0.5f)).direction, out hit, 10, _pickUpLayers))
			{
				return;
			}
			if (!hit.transform.GetComponentInParent<EmotionalObject>() || hit.transform.GetComponentInParent<EmotionalObject>().Rigidbody.isKinematic)
			{
				return;
			}
			_holding = hit.transform.GetComponent<EmotionalObject>();
			_holding.PickUp();
		}
		if (_holding && !Input.GetMouseButton(0) && !_holding.IsRecording)
		{
			_holding.Drop();
			_holding = null;
		}
		if (_holding && Input.GetMouseButtonDown(1) && !_holding.IsRecording && !_emotion.AnalysisActive)
		{
			if (_holding.enabled)
			{
				_emotion.StartAnalysis();
				_holding.Recording();
			}
		}
	}

	private void FixedUpdate()
	{
		if (_holding)
		{
			var newPosition = _cam.ScreenToWorldPoint(new Vector2(Screen.width * 0.5f, Screen.height * 0.5f)) + (_cam.transform.forward * 3);
			var force = newPosition - _holding.transform.position;
			_holding.Rigidbody.velocity = force.normalized * _holding.Rigidbody.velocity.magnitude;
			_holding.Rigidbody.AddForce(force * 100);
			_holding.Rigidbody.velocity *= Mathf.Min(1.0f, force.magnitude / 2);
		}
	}

	public void Die()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}
