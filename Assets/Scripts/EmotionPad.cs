﻿using System.Collections.Generic;
using UnityEngine;

public class EmotionPad : MonoBehaviour {

	[SerializeField]
	private List<LevelObject> _levelObjects;
	[SerializeField]
	private List<GameObject> _padLines;
	[SerializeField]
	private Color _requiredColor;
	private bool _triggered;
	private Material _floor;
	private Color _floorColor;
	public bool Triggered
	{
		get { return _triggered; }
	}

	private void Start()
	{
		foreach (var line in _padLines)
		{
			var mat = line.GetComponent<MeshRenderer>().material;
			mat.color = _requiredColor;
		}
		foreach (Transform child in transform)
		{
			if (child.name == "Wall")
			{
				var mat = child.GetComponent<MeshRenderer>().material;
				mat.color = _requiredColor;
			}
			else if (child.name == "Floor")
			{
				_floor = child.GetComponent<MeshRenderer>().material;
				_floorColor = _floor.color;
			}
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Ball"))
		{
			if (other.GetComponent<MeshRenderer>().material.name.Contains("EmotionBall"))
			{
				BallOnPad(other.gameObject);
			}
			else
			{
				other.GetComponent<EmotionalObject>().SetPad(this);
			}
		}
	}

	public void BallOnPad(GameObject ball)
	{
		_floorColor += ball.GetComponent<MeshRenderer>().material.color;
		_floorColor = new Color(_floorColor.r < 0 ? 0 : _floorColor.r, _floorColor.g < 0 ? 0 : _floorColor.g, _floorColor.b < 0 ? 0 : _floorColor.b, 1);
		_floor.color = new Color(Mathf.Clamp(_floorColor.r, 0, 1), Mathf.Clamp(_floorColor.g, 0, 1), Mathf.Clamp(_floorColor.b, 0, 1), 1);
		if (_floor.color == _requiredColor)
		{
			_triggered = true;
			_levelObjects.ForEach(o => o.Enable());
		}
		else
		{
			if (_triggered)
			{
				_triggered = false;
				_levelObjects.ForEach(o => o.Disable());
			}
		}
		ball.GetComponent<EmotionalObject>().SetPad(this);
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Ball"))
		{
			if (other.GetComponent<MeshRenderer>().material.name.Contains("EmotionBall"))
			{
				BallOffPad(other.gameObject);
			}
			else
			{
				other.GetComponent<EmotionalObject>().SetPad(null);
			}
		}
	}

	public void BallOffPad(GameObject ball)
	{
		_floorColor -= ball.GetComponent<MeshRenderer>().material.color;
		_floorColor = new Color(_floorColor.r < 0 ? 0 : _floorColor.r, _floorColor.g < 0 ? 0 : _floorColor.g, _floorColor.b < 0 ? 0 : _floorColor.b, 1);
		_floor.color = new Color(Mathf.Clamp(_floorColor.r, 0, 1), Mathf.Clamp(_floorColor.g, 0, 1), Mathf.Clamp(_floorColor.b, 0, 1), 1);
		if (_floor.color != _requiredColor)
		{
			_triggered = false;
			_levelObjects.ForEach(o => o.Disable());
		}
		else
		{
			if (!_triggered)
			{
				_triggered = true;
				_levelObjects.ForEach(o => o.Enable());
			}
		}
		ball.GetComponent<EmotionalObject>().SetPad(null);
	}
}
