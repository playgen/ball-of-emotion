﻿using System;

using PlayGen.Assets.SEWA.Analyser;
using PlayGen.Assets.SEWA.Analyser.AudioAnalyser;
using UnityEngine;

public class EmotionCheck : MonoBehaviour {

	private MicAnalyser _micAnalyser;
	public static event Action<Emotion> EmotionDetected = delegate { };
	private bool _analysisActive;
	public bool AnalysisActive
	{
		get { return _analysisActive; }
	}

	private void Awake()
	{
		var monobehaviourContext = this;
		var audioAnalyser = new CommandlineClient(monobehaviourContext);
		_micAnalyser = new MicAnalyser(5000, 5000, monobehaviourContext, audioAnalyser, OnAnalysisCompleted);
	}

	public void StartAnalysis()
	{
		_micAnalyser.StartAnalysis();
		_analysisActive = true;
	}

	public void RecordingFinished()
	{
		_micAnalyser.StopAnalysis();
	}

	private void OnDisable()
	{
		_micAnalyser.StopAnalysis();
	}

	public void OnAnalysisCompleted(AnalysisResult result)
	{
		var anger = result.Results["Anger"];
		var sad = result.Results["Sadness"];
		var positive = result.Results["Positivity"];
		var emotion = anger > sad ? anger > positive ? Emotion.Anger : Emotion.Positive : sad > positive ? Emotion.Sad : Emotion.Positive;
		EmotionDetected(emotion);
		_analysisActive = false;
	}

	public void FakeEvent(Emotion emotion)
	{
		EmotionDetected(emotion);
	}
}
