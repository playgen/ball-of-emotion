﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class MovingPlatform : LevelObject {

	[SerializeField]
	private List<MeshRenderer> _surfaces;
	[SerializeField]
	private Vector3 _startPoint;
	[SerializeField]
	private Vector3 _endPoint;
	[SerializeField]
	private bool _doNotDisable;
	[SerializeField]
	private bool _hideAtStart;
	[SerializeField]
	private bool _hideAtEnd;
	private bool _atStart = true;

	private void Start()
	{
		_surfaces.ForEach(s => s.material.SetTextureScale("_MainTex", new Vector2(s.transform.lossyScale.x * 5, s.transform.lossyScale.z * 5)));
	}

	public override void Enable()
	{
		if (_atStart)
		{
			StopAllCoroutines();
			StartCoroutine(EnableMove());
			_atStart = false;
		}
	}

	public override void Disable()
	{
		if (!_atStart && !_doNotDisable)
		{
			StopAllCoroutines();
			StartCoroutine(DisableMove());
			_atStart = true;
		}
	}

	IEnumerator EnableMove()
	{
		_surfaces.ForEach(s => s.enabled = true);
		var currentPos = transform.position;
		var timer = 0f;
		while (timer <= 1)
		{
			timer += Time.smoothDeltaTime;
			transform.position = Vector3.Lerp(currentPos, _endPoint, timer);
			yield return new WaitForEndOfFrame();
		}
		transform.position = _endPoint;
		if (_hideAtEnd)
		{
			_surfaces.ForEach(s => s.enabled = false);
		}
	}

	IEnumerator DisableMove()
	{
		_surfaces.ForEach(s => s.enabled = true);
		var currentPos = transform.position;
		var timer = 0f;
		while (timer <= 1)
		{
			timer += Time.smoothDeltaTime;
			transform.position = Vector3.Lerp(currentPos, _startPoint, timer);
			yield return new WaitForEndOfFrame();
		}
		transform.position = _startPoint;
		if (_hideAtStart)
		{
			_surfaces.ForEach(s => s.enabled = false);
		}
	}
}
