﻿using System.Collections.Generic;
using System.Linq;

using UnityEngine;

public class EmotionalObject : MonoBehaviour {

	private Emotion _emotion;
	private Rigidbody _rigidbody;
	private bool _recording;
	private bool _waiting;
	private EmotionPad _currentPad;

	[SerializeField]
	private List<MeshRenderer> _visibleEmotion;
	[SerializeField]
	private Material _defaultMat;
	[SerializeField]
	private Material _recordingMat;
	[SerializeField]
	private Material _processingMat;

	public Emotion Emotion
	{
		get { return _emotion; }
	}
	public Rigidbody Rigidbody
	{
		get { return _rigidbody; }
	}

	public bool IsRecording
	{
		get { return _recording; }
	}
	public bool IsWaiting
	{
		get { return _waiting; }
	}

	private void Awake()
	{
		_rigidbody = GetComponent<Rigidbody>();
	}

	private void Update()
	{
		if (enabled)
		{
			if (_recording || _waiting)
			{
				_visibleEmotion.ForEach(m => m.material.mainTextureOffset += new Vector2(Time.smoothDeltaTime, Time.smoothDeltaTime));
			}
		}
	}

	private void OnDisable()
	{
		if (_waiting)
		{
			EmotionCheck.EmotionDetected -= SetEmotion;
		}
	}

	public void PickUp()
	{
		if (enabled)
		{
			_visibleEmotion.ForEach(m => m.material.SetFloat("_Outline", 0.1f));
		}
		_rigidbody.useGravity = false;
	}

	public void Recording()
	{
		if (enabled)
		{
			if (_currentPad)
			{
				_currentPad.BallOffPad(gameObject);
			}
			_visibleEmotion.ForEach(m => m.material = _recordingMat);
			_recording = true;
			Invoke("Waiting", 5);
		}
	}

	public void Waiting()
	{
		if (enabled)
		{
			_recording = false;
			_waiting = true;
			EmotionCheck.EmotionDetected += SetEmotion;
			_visibleEmotion.ForEach(m => m.material = _processingMat);
			_visibleEmotion.ForEach(m => m.material.SetFloat("_Outline", _rigidbody.useGravity ? 0 : 0.1f));
		}
	}

	public void SetEmotion(Emotion emotion)
	{
		if (enabled)
		{
			var pad = _currentPad;
			_visibleEmotion.ForEach(m => m.material = _defaultMat);
			SetColor();
			if (pad)
			{
				pad.BallOffPad(gameObject);
			}
			_emotion = emotion;
			SetColor();
			if (pad)
			{
				pad.BallOnPad(gameObject);
			}
			_visibleEmotion.ForEach(m => m.material.SetFloat("_Outline", _rigidbody.useGravity ? 0 : 0.1f));
			_waiting = false;
			EmotionCheck.EmotionDetected -= SetEmotion;
		}
	}

	public void SetPad(EmotionPad pad)
	{
		_currentPad = pad;
	}

	private void SetColor()
	{
		switch (_emotion)
		{
			case Emotion.Anger:
				_visibleEmotion.ForEach(m => m.material.color = Color.red);
				break;
			case Emotion.Sad:
				_visibleEmotion.ForEach(m => m.material.color = Color.blue);
				break;
			case Emotion.Positive:
				_visibleEmotion.ForEach(m => m.material.color = Color.green);
				break;
		}
	}

	public void Drop()
	{
		_rigidbody.useGravity = true;
		if (enabled)
		{
			_visibleEmotion.ForEach(m => m.material.SetFloat("_Outline", 0));
			_recording = false;
		}
	}
}
