﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLogic : MonoBehaviour {

	[SerializeField]
	private DoorLogic _door;
	[SerializeField]
	private List<EmotionPad> _doorPads;
	[SerializeField]
	private string _nextScene;
	private static Vector3 _previousRotation = new Vector3(0, 180, 0);
	private static Vector3 _previousCameraRotation = new Vector3(0, 180, 0);
	private static Vector3 _previousPosition = new Vector3(0, 0, 48.5f);

	private void Awake()
	{
		var player = FindObjectOfType<PickUpObject>();
		var playerPosition = player.transform.position + new Vector3(0, 0, -1.5f);
		player.transform.eulerAngles = _previousRotation + new Vector3(0, 180, 0);
		player.GetComponentInChildren<Camera>().transform.eulerAngles = _previousCameraRotation + new Vector3(0, 180, 0);
		player.transform.position = new Vector3(-_previousPosition.x, playerPosition.y + _previousPosition.y - 1, playerPosition.z + (50 - _previousPosition.z));
	}

	private void Update()
	{
		if (!IsInvoking("PlayerCheck") && !IsInvoking("LoadNextScene"))
		{
			if (!_door.Opening)
			{
				if (!_door.Open && _doorPads.TrueForAll(d => d.Triggered))
				{
					_door.Enable();
				}
				else if (_door.Open && !_doorPads.TrueForAll(d => d.Triggered))
				{
					_door.Disable();
				}
			}
			if (_door.PlayerInArea)
			{
				_door.Disable();
				Invoke("PlayerCheck", 1);
			}
		}
	}

	private void PlayerCheck()
	{
		if (_door.PlayerInArea)
		{
			Invoke("LoadNextScene", 2);
		}
	}

	private void LoadNextScene()
	{
		var player = FindObjectOfType<PickUpObject>();
		_previousRotation = player.transform.eulerAngles;
		_previousCameraRotation = player.GetComponentInChildren<Camera>().transform.eulerAngles;
		_previousPosition = player.transform.position;
		SceneManager.LoadScene(_nextScene);
		
	}
}
