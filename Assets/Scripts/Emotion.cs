﻿public enum Emotion
{
	Null,
	Anger,
	Sad,
	Positive
}