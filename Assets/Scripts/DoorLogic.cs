﻿using System.Collections.Generic;

using UnityEngine;

public class DoorLogic : LevelObject {

	[SerializeField]
	private List<MeshRenderer> _surfaces;
	private bool _open;
	public bool Open
	{
		get { return _open; }
	}
	public bool Opening
	{
		get { return _animation.isPlaying; }
	}
	private bool _playerInArea;
	public bool PlayerInArea
	{
		get { return _playerInArea; }
	}

	[SerializeField]
	private Animation _animation;

	private void Start()
	{
		_surfaces.ForEach(s => s.material.SetTextureScale("_MainTex", new Vector2(s.transform.lossyScale.x * 5, s.transform.lossyScale.z * 5)));
	}

	public override void Enable()
	{
		if (!_open)
		{
			_animation.clip = _animation.GetClip("DoorsOpening");
			_animation.Play();
			_open = true;
		}
	}

	public override void Disable()
	{
		if (_open)
		{
			_animation.clip = _animation.GetClip("DoorsClosing");
			_animation.Play();
			_open = false;
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
		{
			_playerInArea = true;
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
		{
			_playerInArea = false;
		}
	}
}
