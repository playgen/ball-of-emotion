﻿using System.Collections.Generic;
using System.Linq;

using UnityEngine;

public class TurretLogic : MonoBehaviour {

	private EmotionalObject _emotions;
	private bool _held;
	private GameObject _gun;
	private GameObject _eye;
	[SerializeField]
	private GameObject _bulletPrefab;
	private List<GameObject> _bulletPool = new List<GameObject>();
	private bool _rightSweep;
	// Use this for initialization
	void Start () {
		_emotions = GetComponent<EmotionalObject>();
		_emotions.SetEmotion(Emotion.Anger);
		_gun = transform.Find("Body/Gun").gameObject;
		_eye = transform.Find("Body/Eye").gameObject;
	}

	private void Update()
	{
		if (!_held && !_emotions.Rigidbody.useGravity)
		{
			_held = true;
			GetComponentsInChildren<MeshRenderer>().ToList().ForEach(m => m.material.SetFloat("_Outline", 0.1f));
		}
		else if (_held && _emotions.Rigidbody.useGravity)
		{
			_held = false;
			GetComponentsInChildren<MeshRenderer>().ToList().ForEach(m => m.material.SetFloat("_Outline", 0));
		}
		if (!IsInvoking("RecentFire"))
		{
			for (int i = -5; i <= 5; i++)
			{
				var doBreak = false;
				for (int j = -5; j <= 5; j++)
				{
					RaycastHit hit = new RaycastHit();
					var layer = _emotions.Emotion == Emotion.Anger ? LayerMask.GetMask("Player", "Ball") : LayerMask.GetMask("Turret", "Ball");
					if (Physics.Raycast(_eye.transform.position + new Vector3(0, 0, 0.1f), _eye.transform.forward + new Vector3(i * 0.2f * (_rightSweep ? -1 : 1), j * 0.2f * (_rightSweep ? -1 : 1), 0), out hit, 50, layer))
					{
						if (hit.collider.GetComponent<PickUpObject>() && hit.collider.GetComponent<PickUpObject>().enabled)
						{
							Fire(hit.point);
							doBreak = true;
							break;
						}
						if (hit.collider.GetComponentInParent<EmotionalObject>() && hit.collider.GetComponentInParent<EmotionalObject>().enabled)
						{
							if (hit.collider.GetComponentInParent<EmotionalObject>().Emotion != _emotions.Emotion)
							{
								Fire(hit.point);
								doBreak = true;
								break;
							}
						}
					}
				}
				if (doBreak)
				{
					break;
				}
			}
			_rightSweep = !_rightSweep;
		}
	}

	private void Fire(Vector3 hitPosition)
	{
		GameObject bullet = null;
		var emotionColor = _emotions.Emotion == Emotion.Anger ? Color.red : _emotions.Emotion == Emotion.Sad ? Color.blue : Color.green;
		if (_bulletPool.Count < 15)
		{
			bullet = Instantiate(_bulletPrefab, _gun.transform.position + (_gun.transform.forward * 0.15f), Quaternion.Euler(Vector3.zero));
		}
		else
		{
			bullet = _bulletPool[0];
			_bulletPool.RemoveAt(0);
		}
		if (bullet != null)
		{
			bullet.transform.position = _gun.transform.position + (_gun.transform.forward * 0.15f);
			bullet.transform.LookAt(hitPosition);
			bullet.SetActive(true);
			bullet.GetComponent<Rigidbody>().WakeUp();
			bullet.GetComponent<Rigidbody>().velocity = Vector3.zero;
			bullet.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
			bullet.GetComponent<Rigidbody>().AddForce(bullet.transform.forward * 40);
			bullet.GetComponent<MeshRenderer>().material.color = emotionColor;
			bullet.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", emotionColor);
			bullet.GetComponent<BulletLogic>().SetEmotion(_emotions.Emotion);
			_bulletPool.Add(bullet);
			Invoke("RecentFire", 0.25f);
		}
	}

	public void Die()
	{
		_emotions.Rigidbody.constraints = RigidbodyConstraints.None;
		GetComponentsInChildren<MeshRenderer>().ToList().ForEach(m => m.material.color = Color.black);
		GetComponentsInChildren<MeshRenderer>().ToList().ForEach(m => m.material.SetFloat("_Outline", 0f));
		_emotions.enabled = false;
		_emotions.Rigidbody.mass = 4;
		enabled = false;
	}
}
