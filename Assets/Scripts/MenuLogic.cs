﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuLogic : MonoBehaviour {

	private void Start()
	{
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}

	public void LevelOne()
	{
		SceneManager.LoadScene("Scene");
	}

	public void LevelTwo()
	{
		SceneManager.LoadScene("Scene2");
	}

	public void LevelThree()
	{
		SceneManager.LoadScene("Scene3");
	}

	public void Close()
	{
		Application.Quit();
	}
}
