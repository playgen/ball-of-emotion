﻿using UnityEngine;

public abstract class LevelObject : MonoBehaviour {

	public abstract void Enable();
	public abstract void Disable();
}
