﻿using UnityEngine;

public class BulletLogic : MonoBehaviour {

	private Emotion _emotion;

	public void SetEmotion(Emotion em)
	{
		_emotion = em;
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<PickUpObject>() && other.GetComponent<PickUpObject>().enabled)
		{
			other.GetComponent<PickUpObject>().Die();
		}
		else if (other.GetComponentInParent<TurretLogic>() && other.GetComponentInParent<TurretLogic>().enabled)
		{
			other.GetComponentInParent<TurretLogic>().Die();
			other.GetComponentInParent<Rigidbody>().AddForceAtPosition(transform.eulerAngles, transform.position);
		}
		else if (other.GetComponent<EmotionalObject>() && other.GetComponent<EmotionalObject>().enabled)
		{
			other.GetComponent<EmotionalObject>().SetEmotion(_emotion);
		}
		else if (other.GetComponentInParent<EmotionalObject>() && other.GetComponentInParent<EmotionalObject>().enabled)
		{
			other.GetComponentInParent<EmotionalObject>().SetEmotion(_emotion);
		}
		gameObject.SetActive(false);
		GetComponent<Rigidbody>().Sleep();
	}
}
